"""
	SPNet

This is a direct (i.e. quick and dirty) translation of Eugene Izhikevich's
spnet.m Matlab script. It demonstrates a minimal neural network that produces
polychronization (Izhikevich, 2006).  The original script and a PDF of the
paper describing its significance it can be found at:

https://www.izhikevich.org/publications/spnet.htm

Julia translation by David Dahlbom, September, 2019
"""
module SPNet

using Plots
using Random
using JLD

Random.seed!(1);

numsyn = 100;	# number of synapses per neuron
strmax = 10.0; 	# maximal synaptic strength
delmax = 20;    # maximal conduction delay (ms)

ne = 800; 		# number of excitatory neurons
ni = 200; 		# number of inhibitory neurons
n = ne + ni; 	# total number of neurons

# Parameters of neurons (Izhikevich simple model)
a = [0.02 .* ones(ne); 0.1 .* ones(ni)];
d = [8.0 .* ones(ne);  2.0 .* ones(ni)];


# Initialize connections
## Excitatory Connections 
post = Array{Int64, 2}(undef, n, numsyn)
delays = [Int64[] for j ∈ 1:n, k ∈ 1:delmax]
for i ∈ 1:ne
	p = Random.randperm(n) 	#prevents multiple connections to same neuron
	post[i,:] = p[1:numsyn]
	for j ∈ 1:numsyn
		delay = Int(ceil(delmax*Random.rand())) 	# random excitatory delay length
		push!(delays[i, delay], j)
	end
end

## Inhibitory Connections 
for i ∈ (ne+1):n
	p = Random.randperm(n)
	post[i,:] = p[1:numsyn]
	delays[i,1] = collect(1:numsyn) 	# all inhibitory delays are 1 ms
end

## Weights 
s = [6.0 * ones(ne, numsyn); -5.0 * ones(ni, numsyn)] 	# synaptic weights
sd = zeros(n, numsyn) 									# their derivatives

## Link postsynaptic targets to presynaptic weights
pre = [Int64[] for j ∈ 1:n]
aux = [Int64[] for j ∈ 1:n]
for i ∈ 1:ne
	for j ∈ 1:delmax
		for k ∈ 1:length(delays[i,j])
			push!(pre[post[i, delays[i, j][k]]], n*(delays[i, j][k]-1)+i)
			push!(aux[post[i, delays[i, j][k]]], n*(delmax-1-j)+i)
		end
	end
end

stdp = zeros(n, 1001+delmax)
v = fill(-65.0, n)
u = 0.2 .* v
firings = [-delmax 0]

#secs = 60*60*24 		# one day
secs = 1 
for sec ∈ 1:secs
	global stdp
	global v
	global u
	global firings
	println("$(sec)s of $(secs)s")
	for t ∈ 1:1000
		input = zeros(n)
		input[Int(ceil(n*Random.rand()))] = 20.0; 	# random thalamic input
		fired = findall( x -> x >= 30.0, v)
		v[fired] .= -65.0
		u[fired] = u[fired] + d[fired]
		stdp[fired, t+delmax] .= 0.1
		for k ∈ 1:length(fired)
			sd[pre[fired[k]]] = sd[pre[fired[k]]] .+ stdp[n*t .+ aux[fired[k]]]
		end
		firings = vcat(firings, hcat(t .* ones(Int64, length(fired)), fired))
		k = size(firings, 1)
		while firings[k,1] > t - delmax 
			del = delays[firings[k,2], t - firings[k,1] + 1]
			ind = post[firings[k,2], del]
			input[ind] = input[ind] + s[firings[k,2], del]
			sd[firings[k,2], del] = sd[firings[k,2], del] - 1.2 * stdp[ind, t+delmax];
			k = k-1
		end
		v .+= 0.5 .* ( (0.04 .* v .+ 5) .* v .+ 140.0 .- u .+ input)
		v .+= 0.5 .* ( (0.04 .* v .+ 5) .* v .+ 140.0 .- u .+ input)
		u .+= a .* (0.2 .* v .- u)
		stdp[:, t+delmax+1] = 0.95 .* stdp[:, t+delmax]
	end
	global p = scatter(firings[:,1], firings[:,2], 
	  		xlims=(0, 1000), ylims=(0, n),
	  		legend=:none,
	  		markershape=:circle,
	  		markersize=1,
	  		markercolor=:blue,
	  		markerstrokewidth=0)
	stdp[:, 1:delmax+1] = stdp[:, 1001:1001+delmax]
	ind = findall( x -> x > 1001 - delmax, firings[:,1])
	firings = [ -delmax 0; hcat(firings[ind,1] .- 1000, firings[ind,2]) ]
	su = 0.01 .+ (s[1:ne,:] + sd[1:ne,:])
	s[1:ne,:] = max.(0, min.(strmax, su))
	sd .*= 0.9
end

save("./spnetdata.jld",
	 "firings", firings)

end

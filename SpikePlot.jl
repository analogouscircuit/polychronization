"""
Functions for loading and plotting the output of spnet.cpp
"""
module SpikePlot

using Plots

function loadspikes(filename)
	f = open(filename)
	lines = readlines(f)
	ts = [parse(Int64, split(line, "  ")[1]) for line ∈ lines]
	ns = [parse(Int64, split(line, "  ")[2]) for line ∈ lines]
	return [ts ns]
end

function plotspikes(spikes)
	scatter(spikes[:,1], spikes[:,2],
			markerstrokewidth=0.0,
			markercolor=:blue,
			markersize=1.0,
		    legend=:none)
end

end
